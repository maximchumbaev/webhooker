from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType

from .models import ApiKey, Webhook


class NewUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("username", "password1", "password2")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.is_staff = True

        if commit:
            user.save()

        webhook_content_type = ContentType.objects.get_for_model(Webhook)
        api_key_content_type = ContentType.objects.get_for_model(ApiKey)
        permissions = Permission.objects.filter(
            codename__in=(
                'view_webhook',
                'delete_webhook',
                'view_apikey',
                'add_apikey',
                'change_apikey',
                'delete_apikey'
            ),
            content_type__in=(webhook_content_type, api_key_content_type),
        )
        user.user_permissions.set(list(permissions))
        user.save()

        return user
