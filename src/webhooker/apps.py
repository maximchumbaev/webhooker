from django.apps import AppConfig


class WebhookerConfig(AppConfig):
    name = 'webhooker'
