import json

from datetime import datetime
from http import HTTPStatus
from json.decoder import JSONDecodeError
from uuid import uuid4

from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http.request import HttpRequest
from django.http.response import (
    Http404,
    HttpResponse,
    HttpResponseForbidden,
    JsonResponse,
)
from django.shortcuts import redirect, render

from .forms import NewUserForm
from .models import ApiKey, Webhook


def main(request: HttpRequest, username: str, path: str):
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        raise Http404()

    data = None
    if request.POST:
        data = ''
        for key, value in request.POST.items():
            data += f'"{key}": "{value}"\n'
    elif request.body:
        data = str(request.body)

    Webhook.objects.create(
        id=uuid4(),
        user=user.username,
        requested_at=datetime.now(),
        method=request.method,
        path=path,
        headers=dict(request.headers),
        data=data,
    )
    return HttpResponse(status=HTTPStatus.OK)


def register_request(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("/admin")
        messages.error(
            request,
            "Unsuccessful registration. Invalid information."
        )
    form = NewUserForm()
    return render(
        request=request,
        template_name="main/register.html",
        context={"register_form": form}
    )


def main_page(request):
    user = request.headers.get('x-client-user')
    api_key = request.headers.get('x-client-api-key')
    if user and api_key:
        if ApiKey.objects.filter(key=api_key, user=user).exists():
            webhooks = Webhook.objects.filter(
                user=user,
                received_to_client=False
            )
            data = [
                {
                    'path': webhook.path,
                    'method': webhook.method,
                    'headers': webhook.headers,
                    'data': webhook.data,
                }
                for webhook in webhooks
            ]
            webhooks.update(received_to_client=True)
            return JsonResponse({'data': data})
        return HttpResponseForbidden()

    today_start = datetime.now().replace(hour=0, minute=0, second=0)
    count = Webhook.objects.filter(requested_at__gt=today_start).count()
    return render(
        request=request,
        template_name="main/index.html",
        context={'webhooks_count': count}
    )
