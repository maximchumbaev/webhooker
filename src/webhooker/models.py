from django.db import models


class Webhook(models.Model):
    id = models.UUIDField(primary_key=True)
    user = models.CharField(max_length=150)
    requested_at = models.DateTimeField()
    method = models.CharField(max_length=8)
    path = models.CharField(max_length=255)
    headers = models.JSONField(null=False)
    data = models.TextField(null=True, blank=True)
    received_to_client = models.BooleanField(
        null=False,
        blank=True,
        default=False
    )

    class Meta:
        ordering = ('-requested_at',)


class ApiKey(models.Model):
    key = models.UUIDField(primary_key=True)
    user = models.CharField(max_length=150)
    title = models.CharField(max_length=64)
