from uuid import uuid4

from django.contrib import admin
from django.forms import ValidationError

from .models import ApiKey, Webhook


@admin.register(Webhook)
class WebhookAdmin(admin.ModelAdmin):
    list_display = (
        'method',
        'path',
        'requested_at',
    )

    def get_queryset(self, request):
        if not request.user.is_superuser:
            user = request.user
            qs = super().get_queryset(request)
            return qs.filter(user=user)
        return super().get_queryset(request)


@admin.register(ApiKey)
class ApiKeyAdmin(admin.ModelAdmin):
    list_display = (
        'key',
        'title'
    )

    def get_queryset(self, request):
        if not request.user.is_superuser:
            user = request.user
            qs = super().get_queryset(request)
            return qs.filter(user=user)
        return super().get_queryset(request)

    def get_changeform_initial_data(self, request):
        return {
            'key': uuid4,
            'user': request.user,
        }

    def save_form(self, request, form, change):
        if form.data['user'] != request.user.username:
            form.add_error('user', ValidationError('Invalid username'))
        return super().save_form(request, form, change)
