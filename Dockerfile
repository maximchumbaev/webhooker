FROM python:3.9-slim AS builder

RUN apt-get -y update && apt-get -y install gcc python3-dev libpq-dev locales && locale-gen "ru_RU.UTF-8" && dpkg-reconfigure locales


ENV VIRTUAL_ENV="/opt/venv"
RUN python -m venv $VIRTUAL_ENV
# Make sure we use the virtualenv:
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt

WORKDIR /app
COPY uwsgi.ini /app/
COPY src/ /app/
ENV PATH="/opt/venv/bin:$PATH"
RUN /app/manage.py collectstatic --noinput
